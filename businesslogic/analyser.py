import logging
from model.article import Article
from businesslogic.normaliser import Normaliser
from model.bm25 import CalculateBM25
from model.cosine_similarity import CosSim
import model.jensen_shannon as jsd
import model.tf_idf as tf_idf
from model.response import Response
import re

logging.basicConfig(level=logging.DEBUG)

class Analyse:
    def __init__(self, request):
        self.__request = request

    def analyse(self):
        data = dict(self.__request)

        response = Response()
        article_list = []
        normalised_article_list = []

        # Normalise
        for key, value in data.items():
            validate = self.__validate_input(key, value)
            if validate == False:
                response.error = "Invalidate JSON key or value"
                return response
            article_list.append(Article(value))


        [normalised_article_list.append(Normaliser(article.text).normalise_text()) for article in article_list]

        # Calculate values and make a response:
        article_a = ",".join(normalised_article_list[0])
        article_b = ",".join(normalised_article_list[1])

        cossim = CosSim(article_a, article_b, ",")
        cossim_value = cossim.calc_cos_sim()
        bm25_value = CalculateBM25(article_a, article_b, ",").calculate_bm25_value()
        tf_idf_value = tf_idf.calculate_tf_idf(article_a, article_b)
        jensen_shannon_distance_value = jsd.jensen_shannon_value(normalised_article_list[0], normalised_article_list[1], 2)

        response.cosine_similarity = cossim_value
        response.bm25 = bm25_value
        response.tf_idf = tf_idf_value
        response.jensen_shannon_distance = jensen_shannon_distance_value

        if self.__check_validation_rule(cossim_value, tf_idf_value, bm25_value, jensen_shannon_distance_value) == True:
            response.similarity = "Similar"
        else:
            response.similarity = "Not-Similar"

        if cossim_value == 0.0 and tf_idf_value == 0.0 and bm25_value == 0.0 or jensen_shannon_distance_value >= 0.65:
            response.comment = "Two articles are probably on a different topic."

        if cossim_value >= 0.90:
            response.comment = "Two articles are probably duplicating each other."

        return response

    def __check_validation_rule(self, cos_sim_value, tf_idf_value, bm25_value, jsd_value):
        cos_sim_baseline = 0.5238
        tf_idf_baseline = 29.242
        bm25_baseline = 3.6163
        jsd_baseline = 0.1
        if cos_sim_value >= cos_sim_baseline or tf_idf_value >= tf_idf_baseline or bm25_value >= bm25_baseline or jsd_value <= jsd_baseline:
            return True
        else:
            return False

    def __validate_input(self, key, value):
        try:
            # Length:
            if len(key) != 9:
                print(len(key))
                raise ValueError
            if len(value) >= 20000 or len(value) <= 50:
                raise ValueError
            # content:
            if not re.search("[article\\-][1,2]", key):
                raise ValueError
            return True
        except ValueError:
            logging.error("Raised ValueError")
            return False