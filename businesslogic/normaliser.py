import estnltk
from model.article import Article

class Normaliser:
    def __init__(self, string_a):
        self.__text = string_a
        self.__included_word_types = [
            "nimisona",
            "maarsona",
            "tegusona",
            "omadussona",
            "parisnimi"
        ]

    def normalise_text(self) -> list:
        article = Article(self.__text)
        lemmas = estnltk.Text(article.text).tokenize_words().lemmas
        normalised_text = []
        for item in lemmas:
            if self.__get_word_type_as_name(item) in self.__included_word_types:
                normalised_text.append(item)
        return normalised_text

    def __get_word_type_as_name(self, word):
        # https://estnltk.github.io/estnltk/1.4/tutorials/morf_tables.html
        pos_tag = self.__get_postag_of_word(word)
        types_map = {
            "A": "omadussona_algvorre",
            "C": "omadussona_keskvorre",
            "D": "maarsona",
            "G": "genitiiv_atribuut",
            "H": "parisnimi",
            "I": "hyydsona",
            "J": "sidesona",
            "K": "kaassona",
            "N": "pohiarvsona",
            "O": "jargarvsona",
            "P": "asesona",
            "S": "nimisona",
            "U": "omadussona",
            "V": "tegusona",
            "X": "sona_verbi_juures_tahendus_puudub",
            "Y": "lyhend",
            "Z": "lausemark"
        }
        try:
            if "|" in pos_tag:
                return types_map[list(pos_tag)[0]] # simplifying analysis by removing parallel word types
            else:
                return types_map[pos_tag]
        except TypeError:
            pass


    def __get_postag_of_word(self, word):
        try:
            # 1.4.1:
            t = estnltk.Text(word)
            response = (t.postags)[0]
            return response
        except IndexError:
            pass

