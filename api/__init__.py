from flask_api import FlaskAPI, status
from flask import request, jsonify
from businesslogic.analyser import Analyse
import logging

logging.basicConfig(level=logging.ERROR)
app = FlaskAPI(__name__)

@app.route('/api/1.0/similar-news/', methods=['POST'])
@app.errorhandler(Exception)
def similar_news():
    response = ""
    try:
        if request.method == 'POST':
            req = request.get_json()
            response = Analyse(req).analyse()
            if response.error == None:
                return response.json(), status.HTTP_200_OK
            else:
                return response.json(), status.HTTP_400_BAD_REQUEST

    except Exception as ex:
        msg = jsonify({'error':'Internal application error. Please try again.'})
        logging.exception("message")
        logging.error(ex)
        return msg, status.HTTP_500_INTERNAL_SERVER_ERROR

@app.errorhandler(400)
def bad_request(error):
    return jsonify({'error':'Bad Request. Probably something went wrong with POST JSON object.'}), 400

@app.errorhandler(403)
def forbidden(error):
    return jsonify({'error': 'Forbidden.'}), 403

@app.errorhandler(404)
def not_found(error):
    return jsonify({'error':'Resource not found.'}), 404

@app.errorhandler(408)
def request_timeout(error):
    return jsonify({'error': 'Request timed out.'}), 408

if __name__ == "__main__":
    app.run(debug=True)