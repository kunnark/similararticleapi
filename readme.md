# Similar News API Prototype v1.0

Similar News API Prototype takes input 2 news texts (in Estonian) and gives an estimate whether two comparing news texts are either duplicates, similar, not similar.

This API prototype is a result of [Kunnar Kukk](https://www.linkedin.com/in/kunnarkukk/)’s diploma thesis in Tallinn Technical University IT College.

## **Installation**

1.  Create environment for Python 3.5
2.  Install required packages
3.  Run /api/\__init__.py
 
## Requirements

-   Python 3.5   
-   Gensim 3.4.0 
-   EstNLTK 1.4.1 
-   Flask-API 2.0
Look for more at *environments.yaml*

## **Sample Query**
### **POST /api/1.0/similar-news/**

    {  
    	  "article-1":"Öö vastu esmaspäeva toob saartele ja rannikule tormituult Ilmateenistuse teatel liigub esmaspäeval aktiivne madalrõhkkond Norra rannikule ja laieneb jõuliselt üle Läänemere, tuues Eesti saartele ja rannikule tugeva tormini küündivaid tuuleiile. Ööl vastu esmaspäeva pilvisus tiheneb. Saartelt levib lörtsi- ja vihmasadu mandrile. Hommikul sajab Ida-Eestis paiguti ka lund ja kohati on sadu tugev. Puhub lõunatuul sisemaal 8-13, puhanguti kuni 20 m/s. Saartel ja rannikul on tuule kiirus 13-18, puhanguti 24-27 m/s. Õhutemperatuur on 0 kuni +5 kraadi.Peipsi järvel tugevneb öösel lõunatuul 9-14, puhanguti kuni 20 m/s. Lainekõrgus on 1,4-2 meetrit. Vastu hommikut hakkab sadama lörtsi ja vihma, lühiajaliselt võib sadada ka lund. Nähtavus on mõõdukas, kuid sajus halb. Õhutemperatuur on 0 kuni +2 kraadi.",  
    	  "article-2":"Nädalavahetus toob lund, lörtsi ja vihma Teisipäeval on õhk niiske ja soe, kohati sajab veidi vihma. Hommikupoolikul piirab nähtavust udu. Öösel on Eestis valdavalt pilves ilm. Kohati tibutab vihma, mitmel pool tekib udu. Puhub kagu- ja lõunatuul 4-10, rannikul puhanguti kuni 14 m/s. Sooja on 3 kuni 8 kraadi. Teisipäeva hommik on samuti kohati vähese vihmaga ja mitmel pool udune. Puhub kagu- ja lõunatuul 4-10, rannikul puhanguti kuni 14 m/s. Sooja on 3 kuni 8 kraadi. Päev on pilves selgimistega. Kohati, peamiselt Lääne-Eestis tibutab vihma. Udu peaks lõuna paiku hajuma. Puhub lõuna- ja kagutuul 4-10, rannikul puhanguti kuni 14 m/s. Sooja on 5 kuni 9 kraadi."  
       }

## **Response**

    { 
	    "bm25": "4.324077972348723", 
	    "comment": "None", 
	    "cosine_similarity:": "0.5547981772897465", 
	    "jensen_shannon_distance": "0.001555494498461485", 
	    "similarity_estimate": "Similar", 
	    "tf_idf": "36.758" 
    }

