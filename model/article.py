class Article:
    def __init__(self, text):
        self.__text = str(text)

    def __repr__(self):
        pointer = 0
        if len(self.text) < 30:
            pointer = len(self.text)
            dots = ""
        else:
            pointer = 30
            dots = "..."
        return str(self.text)[0:pointer]+dots

    @property
    def text(self):
        text = self.__text
        return text

    @text.setter
    def text(self, text):
        self.__text = str(text)