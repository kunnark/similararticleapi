from flask import jsonify

class Response:
    def __init__(self):
        self.__cosine_similarity = None
        self.__tf_idf = None
        self.__bm25 = None
        self.__similarity = None
        self.__comment = None
        self.__error = None
        self.__jensen_shannon_distance = None

    @property
    def bm25(self):
        bm25 = self.__bm25
        return bm25

    @bm25.setter
    def bm25(self, bm25):
        self.__bm25 = bm25

    @property
    def cosine_similarity(self):
        cossim = self.__cosine_similarity
        return cossim

    @cosine_similarity.setter
    def cosine_similarity(self, cos_sim):
        self.__cosine_similarity = cos_sim

    @property
    def tf_idf(self):
        return self.__tf_idf

    @tf_idf.setter
    def tf_idf(self, tf_idf):
        self.__tf_idf = tf_idf

    @property
    def jensen_shannon_distance(self):
        return self.__jensen_shannon_distance

    @jensen_shannon_distance.setter
    def jensen_shannon_distance(self, jensen_shannon_distance):
        self.__jensen_shannon_distance = jensen_shannon_distance

    @property
    def similarity(self):
        return self.__similarity

    @similarity.setter
    def similarity(self, similarity):
        self.__similarity = similarity

    @property
    def comment(self):
        return self.__comment

    @comment.setter
    def comment(self, comment):
        self.__comment = comment

    @property
    def error(self):
        return self.__error

    @error.setter
    def error(self, error):
        self.__error = error


    def json(self):
        if self.cosine_similarity == None or self.bm25 == None or self.tf_idf == None:
            return jsonify({
                "error": str(self.error)
            })
        else:
            return jsonify({
                "cosine_similarity:": str(self.cosine_similarity),
                "bm25": str(self.bm25),
                "tf_idf": str(self.tf_idf),
                "jensen_shannon_distance":str(self.jensen_shannon_distance),
                "similarity_estimate": str(self.similarity),
                "comment": str(self.comment)
            })


