import numpy
from gensim.corpora import Dictionary
from gensim.models import ldamodel
from gensim.matutils import jensen_shannon
import logging

logging.basicConfig(filename='../log/lda_model.log', format='%(asctime)s : %(levelname)s : %(message)s', level=logging.ERROR)

def jensen_shannon_value(text_1, text_2, num_topics):
    '''
    Sample code idea: https://radimrehurek.com/gensim/auto_examples/tutorials/run_distance_metrics.html
    :param text_1: list
    :param text_2: list
    :return: float
    '''
    texts = [text_1, text_2]
    dictionary = Dictionary(texts)
    corpus = [dictionary.doc2bow(text) for text in texts]
    numpy.random.seed(1)  # setting random seed to get the same results each time.
    model = ldamodel.LdaModel(corpus, id2word=dictionary, num_topics=num_topics, minimum_probability=1e-8, random_state=1)

    bow_text_1 = model.id2word.doc2bow(text_1)
    bow_text_2 = model.id2word.doc2bow(text_2)

    lda_bow_text_1 = model[bow_text_1]
    lda_bow_text_2 = model[bow_text_2]
    return jensen_shannon(lda_bow_text_1, lda_bow_text_2)