from collections import Counter
from math import sqrt
from math import pow

class CosSim:
    def __init__(self, string1, string2, delimitter):
        self.__string1 = string1
        self.__string2 = string2
        self.__delimitter = delimitter

    def calc_cos_sim(self) -> float:
        nominator = self.__nominator()
        denominator = self.__denominator_part(self.__string1) * self.__denominator_part(self.__string2)
        return float(nominator/denominator)

    def __nominator(self):
        string1_vector = self.__count_word_occurences(self.__string1)
        string2_vector = self.__count_word_occurences(self.__string2)
        n = sum(list(map(lambda word: string1_vector[word] * string2_vector[word], string1_vector)))
        return n

    def __denominator_part(self, string):
        string_vector = self.__count_word_occurences(string)
        d = sqrt(sum(list(map(lambda word: pow(string_vector[word], 2), string_vector))))
        return d

    def __count_word_occurences(self, string) -> Counter:
        return Counter((string).split(self.__delimitter))

